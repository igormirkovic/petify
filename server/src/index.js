const express = require('express');
const { ApolloServer } = require('apollo-server-express');
const cors = require('cors');
const dotEnv = require('dotenv');

const typeDefs = require('./typedefs');
const resolvers = require('./resolvers');
const { connection } = require('./db/utils');
const { verifyUser } = require('./context');


dotEnv.config();
const app = express();
app.use(cors());
app.use(express.json());
connection().then(() => {});

const apolloServer = new ApolloServer({
    typeDefs,
    resolvers,
    context: async ({ req }) => {
            await verifyUser(req)
            return {
                user: req.user
            }
    },
    formatError: (error) => {
        return {
            message: error.message
        }
    }
})

apolloServer.applyMiddleware({app, path: '/graphql'})


app.listen(process.env.PORT || 8001, () =>{
    console.log(`Server listening on ${process.env.PORT}`);
    console.log(`Graphql endpoint on ${apolloServer.graphqlPath}`);
})

