const jwt = require('jsonwebtoken');

const User = require('../db/models/User');

module.exports.verifyUser = async (req) => {
    req.user = null;
    if(req.headers.auth_token){
        try{
            const payload = await jwt.verify(req.headers.auth_token, process.env.JWT_SECRET_KEY)
            req.user = await User.findOne({email: payload.email}, {password: 0})
            if(!req.user){
                throw new Error("Access denied");
            }
        }catch (e) {
            throw new Error("Access denied");
        }

    }
}