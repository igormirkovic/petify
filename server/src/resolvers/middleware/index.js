const { skip } = require('graphql-resolvers');

const Pet = require('../../db/models/Pet');

module.exports.isAuthenticated = async ( _, __ ,{ user }) =>{
    if(!user){
        throw new Error('Access denied.');
    }
    return skip
}

module.exports.isPetOwner = async (_,{id},{user}) =>{
    const pet = await Pet.findOne({$and: [{id:id}, {user: user.id}]});
    console.log(pet);
    if(pet){
        return skip;
    }
    throw new Error('Not authorized');
}
