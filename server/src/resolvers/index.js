const UserResolver = require('./User');
const AnimalResolver = require('./Animal');
const PetResolver = require('./Pet');


module.exports = [
    UserResolver,
    AnimalResolver,
    PetResolver
]