const Animal = require('../db/models/Animal');


module.exports = {
    Query: {
        GetAnimals: async () => {
            return await Animal.find();
        }
    },
    Mutation: {
        NewAnimal: async (_, {input})=> {
            try{
                if(await Animal.findOne({ $and: [{name: input.name}, {race: input.race}]})){
                    throw new Error('Animal with that race exists.');
                }
                return await new Animal({...input}).save();
            }catch (e) {
                throw e;
            }
        },
        EditAnimal: async (_, {id, input}) =>{
            try{
                if(await Animal.findById(id)){
                    return await Animal.findByIdAndUpdate(id,{...input},{new: true});
                }
                throw new Error("Animal doesn't exists.")
            }catch (e) {
                throw e;
            }
        },
        DeleteAnimal: async (_, {id}) => {
            try{
                return await Animal.findByIdAndDelete(id);
            }catch (e) {
                throw e;
            }
        }
    }
}