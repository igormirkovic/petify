const {combineResolvers} = require('graphql-resolvers');

const {isAuthenticated, isPetOwner} = require('./middleware');
const Pet = require('../db/models/Pet');
const User = require('../db/models/User')
const Animal = require('../db/models/Animal')



module.exports = {
    Query: {
        GetPets: combineResolvers(isAuthenticated, async (_, __, { user } ) => {
            return await Pet.find({user: user.id});
        }),
        GetPet: combineResolvers(isAuthenticated, async (_, {id}) => {
            return await Pet.findById(id);
        })
    },
    Mutation: {
        NewPet: combineResolvers(isAuthenticated, async (_, {input}, { user }) => {
            try {
                const findedUser = await User.findOne({email: user.email});
                const newPet = await new Pet({...input, user: findedUser.id}).save();
                findedUser.pets.push(newPet.id);
                await findedUser.save();
                return newPet;
            } catch (e) {
                throw e;
            }
        }),
        EditPet: combineResolvers(isAuthenticated, isPetOwner, async (_, {id, input}, {user}) => {
            try {
                if (await Pet.findById(id)) {
                    return await Pet.findByIdAndUpdate(id, {...input}, {new: true});
                }
                throw new Error("Pet doesn't exists.")
            } catch (e) {
                throw e;
            }
        })
    },
    Pet: {
        animal: async ( {animal} ) =>{
            try{
                return await Animal.findById(animal);
            }catch (e) {
                throw e;
            }
        },
        user: async ( {user} ) =>{
            try{
                return await User.findById(user)
            }catch (e) {
                throw e;
            }
        }
    }
}