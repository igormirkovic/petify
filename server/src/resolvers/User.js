const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { combineResolvers } = require('graphql-resolvers');

const { isAuthenticated } = require('./middleware');


const User = require('../db/models/User');
const Pet = require('../db/models/Pet');


module.exports = {
    Query: {
        GetUser: async (_, __, {user}) => {
            try {
                const NewUser = await User.findById(user.id);
                if(!NewUser){
                    throw new Error('User not exists.');
                }
                return NewUser;
            } catch (e) {
                throw e;
            }
        },
        GetUsers: combineResolvers(isAuthenticated, async (_,__,{ email, id }) => {
            try {
                return await User.find();
            } catch (e) {
                throw e;
            }
        })
    },
    Mutation: {
        SignUp: async (_, {input}) => {
            try {
                if (await User.findOne({email: input.email})) {
                    throw new Error('User exists');
                }
                return await new User({...input, password: await bcrypt.hash(input.password, 12)}).save();
            } catch (e) {
                throw e;
            }
        },
        LogIn: async (_, {input}) => {
            try {
                const user = await User.findOne({email: input.email});
                if(!user){
                    throw new Error('Please, sign up.');
                }
                if(await bcrypt.compare(input.password, user.password)){
                    return {
                        token: jwt.sign({email: user.email}, process.env.JWT_SECRET_KEY, {expiresIn: '7d'})
                    }
                }else{
                    throw new Error('Wrong password.')
                }
            } catch (e) {
                throw e;
            }
        }
    },
    User: {
        pets: async ({ id }) =>{
            try{
                return await Pet.find({user: id});
            }catch (e) {
                throw e;
            }
        }
    }
}