const { gql } = require('apollo-server-express');

module.exports = gql`
    type Pet{
       id: ID!
       name: String
       animal: Animal!
       age: String!
       img: String
       gender: String!
       user: User!
    }
    
    input NewPetInput{
       name: String!
       animal: ID!
       age: String!
       gender: String!
       img: String!
    }
    
    input EditPetInput{
       name: String
       animal: ID
       age: String
       gender: String
    }
    
    extend type Query{
       GetPet(id: ID!): Pet!
       GetPets: [Pet!]
    }
   
    extend type Mutation{
      NewPet(input: NewPetInput): Pet!
      EditPet(id: ID!,input: EditPetInput): Pet!
    }
`;