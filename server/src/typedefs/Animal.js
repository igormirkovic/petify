const { gql } = require('apollo-server-express');


module.exports = gql`
    type Animal{
       id: ID!
       name: String!
       race: String!
    }
    
    input NewAnimalInput{
       name: String!
       race: String!
    }
    
    extend type Query{
       GetAnimal(id: ID!): Animal!
       GetAnimals: [Animal!]
    }
   
    extend type Mutation{
      NewAnimal(input: NewAnimalInput): Animal!
      EditAnimal(id:ID!, input: NewAnimalInput): Animal!
      DeleteAnimal(id:ID!): Animal!
    }
`;