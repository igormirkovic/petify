const { gql } = require('apollo-server-express');

module.exports = gql`
    type User{
       id: ID!
       name: String!
       email: String!
       pets: [Pet!]
    }
    
    type Token{
       token: String!
    }
    
    input SignUpInput{
       name: String!
       email: String!
       password: String!
    }
    
    input LogInInput{
       email: String!
       password: String!
    }

    extend type Query{
       GetUser: User!
       GetUsers: [User]
    }
   
    extend type Mutation{
      SignUp(input: SignUpInput): User
      LogIn(input: LogInInput): Token
    }
`;