const { gql } = require('apollo-server-express');

const UserType = require('./User');
const AnimalType = require('./Animal');
const PetType = require('./Pet');


const RootType = gql`
    type Query{
       _:String
    }
    type Mutation{
       _:String    
    }
    type Subscription{
      _:String
    }
`

module.exports = [
    RootType,
    UserType,
    AnimalType,
    PetType
]
