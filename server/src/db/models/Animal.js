const mongoose = require('mongoose');
const AnimalSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    race: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('Animal', AnimalSchema);