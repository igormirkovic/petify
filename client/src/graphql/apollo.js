import { ApolloClient, InMemoryCache } from '@apollo/client';
import { ApolloLink } from 'apollo-link';
import { setContext } from 'apollo-link-context';
import { createUploadLink } from 'apollo-upload-client';
import { onError } from 'apollo-link-error';

import toast from '../utils/toast'

const link = createUploadLink({ uri: `http://165.227.101.164:8000/graphql` });

const authRetryLink = onError(
    (error) => {
        if(error){
            if (error.graphQLErrors && error.networkError && error.networkError.statusCode && error.networkError.statusCode === 500) {
                localStorage.removeItem('auth_token');
            }
            error.graphQLErrors.map(err => { toast.error(err.message)})
        }
    },
);

const authLink = setContext((_, { headers }) => {
    const token = localStorage.getItem('auth_token');
    return {
        headers: {
            ...headers,
            auth_token: token ? `${token}` : "",
        }
    }
});

export default new ApolloClient({
    cache: new InMemoryCache(),
    link: ApolloLink.from([authRetryLink, authLink, link]),
});