import { gql } from '@apollo/client';

export const GetUser = gql`
 query getUser {
  GetUser {
    id
    name
    email
    pets{
      id
      name
      img
      animal{
        id
      }
    }
  }
  }
`;


export const GetUsersQuery = gql`
  query GetUsers{
    GetUsers {
      id
      name
      email
    }
  }
`;