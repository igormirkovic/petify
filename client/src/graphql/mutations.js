import { gql } from '@apollo/client';

export const LogInMutation = gql`
  mutation LogIn($input: LogInInput!) {
    LogIn(input: $input) {
      token
    }
  }
`;

export const SignUpMutation = gql`
  mutation SignUp($input: SignUpInput!) {
    SignUp(input: $input) {
      name
      email
    }
  }
`;