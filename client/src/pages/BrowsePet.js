import React, { useState }  from 'react';
import { useQuery } from '@apollo/client';


import { GetUser } from '../graphql/queries';
import PetBox from "../components/PetBox";
import Py_Button from "../components/controllers/Py_Button";

export default function BrowsePet(){
    const { loading, error, data } = useQuery(GetUser);

    if (loading) return null;
    if (error) return `Error! ${error}`;

    const renderPetsBox = () =>{
        let pets = [...data.GetUser.pets];
        pets.push({adding: true});
        return pets.map((pet, index) =>{
            return <PetBox key={"_" + index} pet={pet}/>
        })
    }

    return(
        <div className='browse_pet'>
            <div className='browse_pet_container'>
                <span className='browse_pet_welcome'>Hello {data.GetUser.name}</span>
                <span className='browse_pet_label'>Pick your pet and start socializing</span>
                    <div className='browse_pet_boxes_container'>
                    <div className='browse_pet_boxes'>
                        {renderPetsBox()}
                    </div>
                </div>
                <Py_Button label='Manage'/>
            </div>
        </div>
    );
};