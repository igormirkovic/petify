import React, { useState }  from 'react';
import { useMutation, useQuery } from '@apollo/client';

import { Context } from '../context';
import Py_Textfield from "../components/controllers/Py_Textfield";
import Py_Button from "../components/controllers/Py_Button";
import { LogInMutation } from '../graphql/mutations';
import { useHistory } from "react-router-dom";



export default function LogIn(){
    const { setAuth } = Context();
    const history = useHistory();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [logIn, { data }] = useMutation(LogInMutation, {
        onCompleted: ({LogIn}) =>{
            SaveToken(LogIn.token);
        }
    });

    const SaveToken = (token) =>{
        setAuth(token);
        history.push(`/browse/`);
    }

    const onSignUpLinkClick = () =>{
        history.push(`/signup/`);
    }

    return(
        <div className='login_template'>
            <span className='login_logo_title'>Petify</span>
            <span className='login_logo_subtitle'>Social network for pets!</span>
            <div className='login_form'>
                <Py_Textfield value={email} onChange={setEmail} type='text' label='Email'/>
                <Py_Textfield value={password} onChange={setPassword} type='password' label='Password'/>
                <Py_Button label={'Log in'} onClick={() => logIn({variables: {input: {email,password} }})}/>
            </div>
            <span className='login_signup_text'>
                Don't have account? Click here for <span onClick={() => onSignUpLinkClick()} className='login_signup_link'>Sign up</span>.</span>
            <div className='login_shapes first_shape'/>
            <div className='login_shapes second_shape'/>
        </div>
    );
};