import React, { useState }  from 'react';
import Py_Textfield from "../components/controllers/Py_Textfield";
import Py_Button from "../components/controllers/Py_Button";
import {SignUpMutation} from "../graphql/mutations";
import { useHistory } from "react-router-dom";


import { useMutation } from '@apollo/client';


export default function SignUp(){
    const history = useHistory();
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [signUp, { data }] = useMutation(SignUpMutation, {
        onCompleted: ({LogIn}) =>{
            onSignUpComplete();
        }
    });

    const onSignUpComplete = () =>{
        history.push('/login');
    }

    return(
        <div className='login_template'>
            <span className='login_logo_title'>Petify</span>
            <span className='login_logo_subtitle'>Please, sign up</span>
            <div className='login_form'>
                <Py_Textfield value={name} onChange={setName} type='text' label='Name'/>
                <Py_Textfield value={email} onChange={setEmail} type='text' label='Email'/>
                <Py_Textfield value={password} onChange={setPassword} type='password' label='Password'/>
                <Py_Button label={'Sign up'} onClick={() => signUp({variables: {input: {name,email,password} }})}/>
            </div>
        </div>
    );
};