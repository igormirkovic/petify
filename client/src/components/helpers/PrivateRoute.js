import React from "react";
import {Redirect, Route} from 'react-router-dom';
import {Context} from "../../context";

export const PrivateRoute = ({component: Component, ...props}) => {
    const {getAuth, setAuth} = Context();
    return (
        <Route
            {...props}
            render={() =>
                getAuth ?
                    <Component/>
                    :
                    <Redirect
                        to={{
                            pathname: "/login"
                        }}/>
            }
        >
        </Route>
    )

}