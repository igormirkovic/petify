
import React from 'react';
export default function PetBox({pet}){
    console.log(pet);
    return(
        <div className={pet.adding ? "pet_box pet_box_adding" : "pet_box"}>
            <span className="pet_box_name">{pet.adding ? "" : pet.name}</span>
            {!pet.adding && pet.img ? <img src={pet.img}/> : ""}
        </div>
    );
};