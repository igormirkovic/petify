import React from 'react';

import Button from '@material-ui/core/Button';

export default function Py_Button(props) {

    return (
        <Button onClick={props.onClick} className='py_button' variant='contained' color="primary">
            {props.label}
        </Button>
    );
};