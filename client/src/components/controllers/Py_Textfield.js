import React from 'react';
import TextField from '@material-ui/core/TextField';


export default function Py_Textfield(props){

    return(
        <TextField
            value={props.value}
            label={props.label}
            type={props.type}
            onChange={(e) => props.onChange(e.target.value)}
            variant="outlined"
            className='py_textfield'
        />
    );
};