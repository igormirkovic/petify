import React, { useState } from 'react';
import Context, { ContextDefinition } from './context';

const token = window.localStorage.getItem('auth_token');

const State = (props) => {
    const [getAuth, setAuth] = useState(token);

    const setTokenLS = (newToken) => {
        if (newToken) {
            window.localStorage.setItem('auth_token', newToken);
        } else {
            window.localStorage.removeItem('auth_token');
        }
        setAuth(newToken)
    };

    return (
        <ContextDefinition.Provider
            value={{
                getAuth,
                setAuth: setTokenLS,
            }}
        >
            {props.children}
        </ContextDefinition.Provider>
    );
};

export {
    State,
    Context,
};
