import React from 'react';
import './App.css';
import {ApolloProvider} from '@apollo/client';
import client from './graphql/apollo'
import {Context} from './context';
import { PrivateRoute } from './components/helpers/PrivateRoute'

import LogIn from "./pages/LogIn";
import BrowsePet from "./pages/BrowsePet";
import SignUp from "./pages/SignUp";


import {Switch, Route} from 'react-router-dom';


export default function App() {
    const {getAuth, setAuth} = Context();
    return (
        <div className="App">
            <ApolloProvider client={client}>
                <Route exact path="/signup/" component={SignUp}/>
                <Route exact path="/login/" component={LogIn}/>
                <PrivateRoute exact path="/" component={BrowsePet}/>
                <PrivateRoute exact path="/browse/" component={BrowsePet}/>
            </ApolloProvider>
        </div>
    );
}

